using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(cEFieldOfView))]
public class EFOVEditor : Editor
{
    private void OnSceneGUI()
    {
        cEFieldOfView fov = (cEFieldOfView)target;
        Handles.color = Color.red;
        Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.radius, 5);

        Vector3 viewAngl01 = DirectionFromAngle(fov.transform.eulerAngles.y, -fov.angle / 2);
        Vector3 viewAngl02 = DirectionFromAngle(fov.transform.eulerAngles.y, fov.angle / 2);

        Handles.color = Color.blue;
        Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngl01 * fov.radius, 2);
        Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngl02 * fov.radius, 2);

        if (fov.canSeeEnemy)
        {
            Handles.color = Color.green;
            Handles.DrawLine(fov.transform.position, fov.playerRef.transform.position, 3.5f);
        }
    }

    private Vector3 DirectionFromAngle(float eulerY,float angleInDegrees)
    {
        angleInDegrees += eulerY;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}
