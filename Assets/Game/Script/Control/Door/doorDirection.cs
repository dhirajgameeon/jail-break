using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorDirection : MonoBehaviour
{
    public bool isFront;
    public controlDoor door;
    void Start()
    {
        
    }


    void Update()
    {
        
    }
    private void OnTriggerStay(Collider other)
    {
        if(isFront && other.CompareTag("Player"))
        {
            door.isPlayerFront = true;
        }
        if (!isFront && other.CompareTag("Player"))
        {
            door.isPlayerFront = false;
        }
    }
}
