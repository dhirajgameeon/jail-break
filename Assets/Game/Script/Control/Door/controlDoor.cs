using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class controlDoor : MonoBehaviour
{

    public Stats dStats;
    public Transform Lebal;
    public TextMeshPro Level;
    public Animator anim;
    public bool isDoorOpend = false;
    private Collider boxCollider;

    public bool GameComplete;
    public bool isPlayerNear;

    public ParticleSystem Poof1, Poof2, Poof3;



    [Header("Detect Player")]
    [Space(5)]
    public bool isPlayerFront;
    public LayerMask playerMask;
    public float detectArea;
    public Transform Player;
    void Start()
    {
        boxCollider=GetComponent<Collider>();
        StartCoroutine(death());
        Level.text = "Lv. " + dStats.Level.ToString("N0");

    }

    IEnumerator death()
    {
        WaitForSeconds wait = new WaitForSeconds(0.05f);
        while (!isDoorOpend)
        {
            yield return wait;
            isDeadEnemy(isDoorOpend);
        }
    }
    private void Update()
    {
        if(isDoorOpend && GameComplete && isPlayerNear)
        {
            movePlayerAi.MPA.isPlayerOpenLastDoor = true;
        }
        checkPlayer();
    }
    void isDeadEnemy(bool callback)
    {
        Lebal.forward = -Camera.main.transform.forward;
        if (isDoorOpend && !isPlayerFront)
        {
            AudioManager.am.PlayAudio(AudioLib.PunchDoor);
            playVFX();
            Invoke("poof", 0.2f);
            anim.Play("OpenFront");
            this.gameObject.layer = 9;            
            Invoke("desableObject", 0.1f);
            callback = false;
        }
        if (isDoorOpend && isPlayerFront)
        {
            AudioManager.am.PlayAudio(AudioLib.PunchDoor);
            playVFX();
            Invoke("poof", 0.2f);
            anim.Play("OpenBack");
            this.gameObject.layer = 9;
            Invoke("desableObject", 0.1f);
            callback = false;
        }
    }
    void poof()
    {
        Poof3.Play();
    }

    void playVFX(int type=0)
    {
        if (type == 0)
        {
            //Poof1.Play();
            //Poof2.Play();
        }
        if (type == 1)
        {
            //Poof3.Play();
        }
    }
    void desableObject()
    {
        //this.gameObject.SetActive(false);
        this.Lebal.gameObject.SetActive(false);
    }

    RaycastHit hit;
    void checkPlayer()
    {
        if (Physics.Raycast(transform.position, transform.forward,out hit, detectArea, playerMask))
        {
            Player = hit.transform;
        }
        else
        {
            Player = null;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            isPlayerNear = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear) isPlayerNear = false;
    }
    /*    private void OnCollisionEnter(Collision collision)
        {
            if (this.isDoorOpend && collision.gameObject.CompareTag("Player"))
            {
                GameManager.instance.isGameComplete = true;
                GameManager.instance.Continue.SetActive(true);
            }
        }*/
}
