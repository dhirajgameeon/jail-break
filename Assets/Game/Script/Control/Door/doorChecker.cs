using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorChecker : MonoBehaviour
{
    public int doorOpen;

    public controlDoor door1;
    public controlDoor door2;


    private void OnTriggerStay(Collider other)
    {
        if (door2)
        {
            if (door1.isDoorOpend || door2.isDoorOpend)
            {
                if (other.CompareTag("Player"))
                {
                    GetComponent<Collider>().enabled = false;
                    GameManager.instance.isDoorOpen[doorOpen] = true;
                }
            }
        }
        else 
        {
            if (door1.isDoorOpend && other.CompareTag("Player"))
            {
                GetComponent<Collider>().enabled = false;
                GameManager.instance.isDoorOpen[doorOpen] = true;
            }
        }       
    }
}
