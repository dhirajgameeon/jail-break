using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum AnimState
{
    eIdle,
    idle,
    run,
    attack,
    deance,
    death
}


public class controlAnime : MonoBehaviour
{
    public Animator anime;
    public GameObject[] Character;

    private void Update()
    {
        foreach(var character in Character)
        {
            if (character.activeSelf)
            {
                anime = character.GetComponent<Animator>();
            }
        }
    }
    public void PlayAnime(AnimState animState,float speed,bool loop=true, float delay=0)
    {
        switch (animState)
        {
            case AnimState.idle:
                move(speed);
                break;
            case AnimState.run:
                move(speed);
                break ;
            case AnimState.attack:
                Attack(speed,delay);
                break;
            case AnimState.death:
                death(loop);
                break;
            case AnimState.deance:
                Dance();
                break;
        }
    }
    public void PlayIdleAnim(AnimState animState,string anime=default)
    {
        switch (animState) { 
        case AnimState.eIdle:
                eIdleAnim(anime);
                break;
        }

    }

    public void resetAttackTrigger()
    {

    }
    void move(float m)
    {
        anime.SetFloat("speed", m);
    }
    void Attack(float attackSpeed, float delay=0)
    {
        int x = Random.Range(0, 2);
        if (x == 0)
        {
            anime.SetFloat("attackSpeed", attackSpeed);
            Invoke("onAttack", delay);
        }
        if (x == 1)
        {
            anime.SetFloat("attackSpeed", attackSpeed);
            Invoke("onAttack1", delay);
        }
        
    }
    void onAttack()
    {
        anime.Play("attack");
    }
    void onAttack1()
    {
        anime.Play("attack1");
    }
    void death(bool t)
    {
        if(t)
            anime.SetTrigger("dieF");
        else
            anime.SetTrigger("dieB");
    }
    void Dance()
    {
        if(!anime.GetCurrentAnimatorStateInfo(0).IsName("House Dancing"))
            anime.Play("House Dancing");
    }

    public void eIdleAnim(string animation=default)
    {
        if (animation.Length > 0)
        {
            if (!anime.GetCurrentAnimatorStateInfo(0).IsName(animation))
                anime.Play(animation);
        }
    }
}
