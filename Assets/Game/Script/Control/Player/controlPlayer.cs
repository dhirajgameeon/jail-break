using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

public class controlPlayer : MonoBehaviour
{
    public Stats PlayerStats;
    public float lookAtSpeed = 15;
    public float coolDownTime = 2;
    public float levelCounterSpeed = 50;
    public bool isDied = false;
    public bool isKilledEnemy = false;
    public TextMeshPro levelCount;
    public Transform LevelObject;
    public ParticleSystem punchEffect;

    [Header("Level UI Animation")]
    [Space(5)]
    public Animator LevelAddingAnimation;
    public Animator LevelPopAnimation;
    public TextMeshPro LevelNumber;

    cPFieldOfView fieldOfView;
    controlAnime anime;
    movePlayer movePlayer;
    private float killResetTime = 2;
    private void Start()
    {
        fieldOfView = GetComponent<cPFieldOfView>();
        anime = GetComponent<controlAnime>();
        movePlayer = GetComponent<movePlayer>();
        movePlayer.movementSpeed = PlayerStats.MovementSpeed;
        StartCoroutine(death());
        
        killResetTime = coolDownTime;
    }

    float levelCounter;
    
    private void Update()
    {
        checkIfEnemyInAttackRange();
        //onGameComplete();
        isKilledEnemyData();
        LevelObject.forward = -Camera.main.transform.forward;
        playerLevelControl();
    }

    void onGameComplete()
    {
        if (GameManager.instance.isGameComplete)
            anime.PlayAnime(AnimState.deance, 1);
    }

    
    void isKilledEnemyData()
    {
        if (isKilledEnemy)
        {
            if (killResetTime > 0) killResetTime -= Time.deltaTime;
            if (killResetTime <= 0)
            {
                isKilledEnemy = false;
            }
        }
    }
    IEnumerator death()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        while (!isDied)
        {
            yield return wait;
            isDeadPlayer(!isDied);
        }
    }
    private void isDeadPlayer(bool callback)
    {
        if (isDied)
        {
            AudioManager.am.PlayAudio(AudioLib.EnemyDeath);
            this.gameObject.layer = 0;
            GameManager.instance.Retry.SetActive(true);
            GameManager.instance.isPlayerDead = true;
            anime.PlayAnime(AnimState.death, 0, true);
            callback = false;
        }
    }
    void checkIfEnemyInAttackRange()
    {
        if (fieldOfView.enemyRef)
        {
            GameObject eRef = fieldOfView.enemyRef;
            smoothLookat(eRef.transform, lookAtSpeed);
            playerProgression(eRef);
            //eRef.GetComponent<controlEnemy>().isFacingPlayer = fieldOfView.facingEnemy;
        }
        if(!fieldOfView.enemyRef && fieldOfView.doorRef)
        {
            GameObject dRef = fieldOfView.doorRef;
            //smoothLookat(dRef.transform, lookAtSpeed/4);
            playerProgressionDoor(fieldOfView.doorRef);
        }
    }

    void playerProgression(GameObject eRef)
    {
        if (eRef.GetComponent<controlEnemy>().isFacingPlayer)
        {
            //print("Facing");
            if (PlayerStats.Level >= eRef.GetComponent<controlEnemy>().enemyStats.Level && !eRef.GetComponent<controlEnemy>().isDead)
            {
                //print("High Level");
                if (Vector3.Distance(transform.position, eRef.transform.position) <= PlayerStats.AttackRange)
                {
                    Attack(eRef.GetComponent<controlEnemy>());
                }
            }
            /*if (PlayerStats.Level < eRef.GetComponent<controlEnemy>().enemyStats.Level && !eRef.GetComponent<controlEnemy>().isDead)
            {
                print("Low Level");
                if (Vector3.Distance(transform.position, eRef.transform.position) <= PlayerStats.AttackRange)
                {
                    //Attack(eRef.GetComponent<controlEnemy>());
                    Death();
                }
            }*/
        }
        if (!eRef.GetComponent<controlEnemy>().isFacingPlayer)
        {
            if ( !eRef.GetComponent<controlEnemy>().isDead)
            {
                //print("Null Level");
                if (Vector3.Distance(transform.position, eRef.transform.position) <= PlayerStats.AttackRange)
                {
                    Attack(eRef.GetComponent<controlEnemy>());
                }
            }
        }
       
    }
    void playerProgressionDoor(GameObject dRef)
    {
        if (!dRef.GetComponent<controlDoor>().isDoorOpend && PlayerStats.Level >= dRef.GetComponent<controlDoor>().dStats.Level)
        {
            if (Vector3.Distance(transform.position, dRef.transform.position) <= PlayerStats.AttackRange)
            {
                openDoor(dRef.GetComponent<controlDoor>());
            }
        }
    }
    float nextAttack = 0;
    void Attack(controlEnemy cE)
    {
        if (Time.time >= nextAttack)
        {
            print("<color=red>Attack </color> ");
            anime.PlayAnime(AnimState.attack, PlayerStats.AttackSpeed, false);
            Invoke("punch", 0.25f);
            StartCoroutine(KillEnemy(cE));
            nextAttack = Time.time + 1f / PlayerStats.AttackRate;
        }
    }
    void punch()
    {
        punchEffect.Play();
    }
    IEnumerator KillEnemy(controlEnemy cE)
    {
        WaitForSeconds wait = new WaitForSeconds(0.4f);
        movePlayer.movementSpeed = PlayerStats.movementSpeedWhileAttacking;
        yield return wait;
        print("<color=red> Enemy Killed </color>");
        movePlayer.movementSpeed = PlayerStats.MovementSpeed;
        if (cE.enemyStats.Level <= PlayerStats.Level || !cE.isFacingPlayer)
        {
            PlayerStats.Level += cE.enemyStats.Level;
            LevelNumber.text = "+" + cE.enemyStats.Level.ToString("N0");
            LevelAddingAnimation.Play("credit added");
            popAnimation();

            if (!isKilledEnemy)
            {
                isKilledEnemy = true;
                AudioManager.am.PlayAudio(AudioLib.Punch);
                killResetTime = coolDownTime;
            }
            cE.isDead = true;
        }
        else if(cE.enemyStats.Level > PlayerStats.Level && cE.isFacingPlayer)
        {
            cE.isDead = false;
            isDied = true;
        }
        
    }



    void popAnimation()
    {
        if (!LevelPopAnimation.enabled)
        {
            LevelPopAnimation.enabled = true;
            LevelPopAnimation.Play("UI");
            Invoke("desabelAnimator", LevelPopAnimation.GetCurrentAnimatorStateInfo(0).length);
        }        
    }

    void desabelAnimator()
    {
        LevelPopAnimation.enabled = false;
    }

    float nextTimeOpen = 0;
    void openDoor(controlDoor cD)
    {
        if (Time.time >= nextTimeOpen)
        {
            //print("<color=red>Door Open </color> ");
            anime.PlayAnime(AnimState.attack, PlayerStats.AttackSpeed, false);
            Invoke("punch", 0.25f);
            StartCoroutine(oDoor(cD));
            nextTimeOpen = Time.time + 1f / 0.5f;
        }
    }
    IEnumerator oDoor(controlDoor cD)
    {
        WaitForSeconds wait = new WaitForSeconds(0.3f);
        movePlayer.movementSpeed = PlayerStats.movementSpeedWhileAttacking;
        yield return wait;
        print("<color=red> Door Opened</color>");
        movePlayer.movementSpeed = PlayerStats.MovementSpeed;
        if (cD.dStats.Level <= PlayerStats.Level)
        {
            /*PlayerStats.Level += cD.dStats.Level;
            levelCount.text = PlayerStats.Level.ToString("N0");*/
            cD.isDoorOpend = true;
        }
        else if (cD.dStats.Level > PlayerStats.Level)
        {
            cD.isDoorOpend=false;
        }
    }

    void playerLevelControl()
    {
        if(levelCounter < PlayerStats.Level)
        {
            levelCounter += ((PlayerStats.Level * levelCounterSpeed) * 0.01f) * Time.deltaTime;
            if (levelCounter >= PlayerStats.Level)
            {
                levelCounter = PlayerStats.Level;                
            }
        }
        levelCount.text = "Lv. "+levelCounter.ToString("N0");
    }

    void smoothLookat(Transform target, float rotationSpeed)
    {
        var targetPos = target.position;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(transform.position, Vector3.up, PlayerStats.AttackRange, 1.5f);
    }
#endif  
}
