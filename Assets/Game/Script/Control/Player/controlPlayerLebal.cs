using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPlayerLebal : MonoBehaviour
{
    private cPFieldOfView fov;
    private controlPlayer cP;

    public Color LowLevelColor;
    public Color HighLevelColor;
    public Color IdleLevelColor;
    public float speed = 10;

    public SpriteRenderer LevelIndicator;
    public bool isLevelLow;
    public bool isLevelHigh;
    void Start()
    {
        fov = GetComponent<cPFieldOfView>();
        cP = GetComponent<controlPlayer>();
        IdleLevelColor = LevelIndicator.color;
    }
    void Update()
    {
        checkLevel();
    }

    void checkLevel()
    {
        if(fov.enemyRef && fov.enemyRef.GetComponent<controlEnemy>())
        {
            controlEnemy cE = fov.enemyRef.GetComponent<controlEnemy>();
            if (cP.PlayerStats.Level < cE.enemyStats.Level)
            {
                isLevelLow = true;
                isLevelHigh = false;
                LevelIndicator.color = colorChange(LevelIndicator.color, LowLevelColor);
            }
            if (cP.PlayerStats.Level > cE.enemyStats.Level)
            {
                isLevelLow = false;
                isLevelHigh = true;
                LevelIndicator.color = colorChange(LevelIndicator.color, HighLevelColor);
            }
        }
        else
        {
            isLevelLow = false;
            isLevelHigh = false;
            LevelIndicator.color = colorChange(LevelIndicator.color, IdleLevelColor);
        }
    }
    Color colorChange(Color startColor, Color endColor)
    {
        return Color.Lerp(startColor, endColor, speed * Time.deltaTime);
    }

}
