using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPlayerLevel : MonoBehaviour
{
    private movePlayer move;
    private controlPlayer control;
    public List<GameObject> Mesh = new List<GameObject>();
    public List<int> LevelUp = new List<int>();
    public ParticleSystem upgradePlayer;

    [Tooltip("Value in %")]
    [SerializeField]float movementSpeedUpgradeRate = 1;

    private CharacterController controller;
    void Start()
    {
        control = GetComponent<controlPlayer>();
        move = GetComponent<movePlayer>();
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        levelUp();
    }
    
    public void levelUp()
    {
        if (control.PlayerStats.Level >= 1 && control.PlayerStats.Level <= LevelUp[0])
        {
            foreach (var mesh in Mesh)
            {
                if (mesh.name == "Level 1")
                {
                    if (!mesh.activeSelf)
                    {
                        control.PlayerStats.MovementSpeed += (control.PlayerStats.MovementSpeed * movementSpeedUpgradeRate) * 0.01f;
                        controller.radius = 0.5f;
                        upgradePlayer.Play();
                        mesh.SetActive(true);
                    }
                }
                else
                {
                    mesh.SetActive(false);
                }
            }
        }
        if (control.PlayerStats.Level >= (LevelUp[0]+1) && control.PlayerStats.Level <= LevelUp[1])
        {
            foreach (var mesh in Mesh)
            {
                if (mesh.name == "Level 2")
                {
                    if (!mesh.activeSelf)
                    {
                        control.PlayerStats.MovementSpeed += (control.PlayerStats.MovementSpeed * movementSpeedUpgradeRate) * 0.01f;
                        AudioManager.am.PlayAudio(AudioLib.LevelUp);
                        controller.radius = 1f;
                        upgradePlayer.Play();
                        mesh.SetActive(true);
                    }
                }
                else
                {
                    mesh.SetActive(false);
                }
            }
        }
        if (control.PlayerStats.Level >= (LevelUp[1]+1))
        {
            foreach (var mesh in Mesh)
            {
                if (mesh.name == "Level 3")
                {
                    if (!mesh.activeSelf)
                    {
                        control.PlayerStats.MovementSpeed += (control.PlayerStats.MovementSpeed * movementSpeedUpgradeRate) * 0.01f;
                        AudioManager.am.PlayAudio(AudioLib.LevelUp);
                        controller.radius = 1.5f;
                        upgradePlayer.Play();
                        mesh.SetActive(true);
                    }
                }
                else
                {
                    mesh.SetActive(false);
                }
            }
        }
    }
}
