using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerTutorial : MonoBehaviour
{
    public Transform Arrow;
    public List<Transform> Door = new List<Transform>();
    public Transform Enemy;

    private cPFieldOfView fov;
    private controlPlayer player;

    public bool isDesable = false;
    private void Start()
    {
        fov=GetComponent<cPFieldOfView>();
        player=GetComponent<controlPlayer>();
    }
    private void Update()
    {
        if (!isDesable)
        {
            checkForDoorAndEnemy();
            arrowMark();
        }else
        {
            Arrow.gameObject.SetActive(false);
        }
    }

    void checkForDoorAndEnemy()
    {
        foreach(Transform door in Door.ToArray())
        {
            if (door.GetComponent<controlDoor>().isDoorOpend)
                Door.Remove(door);
        }
/*        foreach (Transform enemy in Enemy)
        {
            if (enemy.GetComponent<controlEnemy>().isDead)
                Door.Remove(enemy);
        }*/

        if (Door.Count <= 0 && !Enemy && Arrow.gameObject.activeSelf)
        {
            Arrow.gameObject.SetActive(false);
        }
    }
    public void arrowMark()
    {
        if (!Enemy.GetComponent<controlEnemy>().isDead)
        {
            Arrow.LookAt(new Vector3(Enemy.position.x, transform.position.y, Enemy.position.z));
        }
        if(!Enemy|| Enemy.GetComponent<controlEnemy>().isDead)
        {
            if (Door.Count > 0)
            {
                if (!Door[0].GetComponent<controlDoor>().isDoorOpend && player.PlayerStats.Level >= Door[0].GetComponent<controlDoor>().dStats.Level)
                {
                    Arrow.gameObject.SetActive(true);
                    Arrow.LookAt(new Vector3(Door[0].position.x, transform.position.y, Door[0].position.z));
                }
                else
                {
                    Arrow.gameObject.SetActive(false);
                }
            }
            else
            {
                Arrow.gameObject.SetActive(false);
            }
          
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RM"))
            isDesable = true;
    }
}
