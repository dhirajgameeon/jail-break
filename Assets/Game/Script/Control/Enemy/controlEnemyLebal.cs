using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEnemyLebal : MonoBehaviour
{
    private cEFieldOfView fov;
    private controlEnemy cE;

    public Color LowLevelColor;
    public Color HighLevelColor;
    public Color IdleLevelColor;
    public float speed = 10;

    public SpriteRenderer LevelIndicator;
    public bool isLevelLow;
    public bool isLevelHigh;
    void Start()
    {
        fov = GetComponent<cEFieldOfView>();
        cE = GetComponent<controlEnemy>();
        IdleLevelColor = LevelIndicator.color;
    }
    void Update()
    {
        checkLevel();
    }

    void checkLevel()
    {
        if (fov.playerRef && fov.playerRef.GetComponent<controlPlayer>())
        {
            controlPlayer cP = fov.playerRef.GetComponent<controlPlayer>();
            if (cE.enemyStats.Level < cP.PlayerStats.Level)
            {
                isLevelLow = true;
                isLevelHigh = false;
                LevelIndicator.color = colorChange(LevelIndicator.color, LowLevelColor);
            }
            if (cE.enemyStats.Level > cP.PlayerStats.Level)
            {
                isLevelLow = false;
                isLevelHigh = true;
                LevelIndicator.color = colorChange(LevelIndicator.color, HighLevelColor);
            }
        }
        else
        {
            isLevelLow = false;
            isLevelHigh = false;
            LevelIndicator.color = colorChange(LevelIndicator.color, IdleLevelColor);
        }
    }
    Color colorChange(Color startColor, Color endColor)
    {
        return Color.Lerp(startColor, endColor, speed * Time.deltaTime);
    }
}
