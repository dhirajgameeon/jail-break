using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
public class controlEnemy : MonoBehaviour
{
    public Stats enemyStats;
    public Transform Lebal;
    public TextMeshPro level;

    public float lookAtSpeed = 75;
    public bool isDead;
    public bool isFacingPlayer;


    cEFieldOfView fieldOfView;
    controlAnime anim;
    float nextAttack = 0;
    void Start()
    {
        fieldOfView = GetComponent<cEFieldOfView>();
        anim = GetComponent<controlAnime>();
        StartCoroutine(death());
        level.text = "Lv. " + enemyStats.Level.ToString("N0");
        nextAttack = Time.time + 1f / enemyStats.AttackRate;
    }

    // Update is called once per frame
    void Update()
    {
        checkEnemyInAttackRange();
    }
    bool active = true;
    IEnumerator death()
    {
        
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        while (active)
        {
            yield return wait;
            isDeadEnemy(active);
        }
    }

    public void checkEnemyInAttackRange()
    {
        Lebal.forward = -Camera.main.transform.forward;
        if (fieldOfView.canSeeEnemy && !isDead)
        {
            GameObject pRef = fieldOfView.playerRef;
            smoothLookat(pRef.transform, lookAtSpeed);
            enemyProgression(pRef);
            isFacingPlayer = fieldOfView.facingEnemy;
        }
        active = !isDead;
    }

    void enemyProgression(GameObject pRef)
    {
        if (isFacingPlayer && pRef.GetComponent<controlPlayer>().PlayerStats.Level >= enemyStats.Level)
        {
            if(Vector3.Distance(transform.position, pRef.transform.position) <= enemyStats.AttackRange)
            {
                //Attack(pRef.GetComponent<controlPlayer>(), false);
            }
        }
       else if (isFacingPlayer && enemyStats.Level > pRef.GetComponent<controlPlayer>().PlayerStats.Level)
        {
            if (Vector3.Distance(transform.position, pRef.transform.position) <= enemyStats.AttackRange)
            {                
                Attack(pRef.GetComponent<controlPlayer>(), true);
            }
        }
    }

   
    private void Attack(controlPlayer controlPlayer, bool isDead)
    {
        if (Time.time >= nextAttack)
        {
            print("<color=Blue>Attack </color> ");
            anim.PlayAnime(AnimState.attack, enemyStats.AttackSpeed, false, 0.1f);
            StartCoroutine(KillEnemy(controlPlayer, isDead));
            nextAttack = Time.time + 1f / enemyStats.AttackRate;
        }
    }
    IEnumerator KillEnemy(controlPlayer cP, bool isDead)
    {
        WaitForSeconds wait = new WaitForSeconds(0.4f);
        AudioManager.am.PlayAudio(AudioLib.Punch);
        yield return wait;
        print("<color=Blue> Enemy Killed </color>");
        cP.isDied = isDead;
    }

    void smoothLookat(Transform target, float rotationSpeed)
    {
        var targetPos = target.position;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }




    void isDeadEnemy(bool callback)
    {
        if (isDead)
        {
            this.gameObject.layer = 9;
            Lebal.gameObject.SetActive(false);
            //boxCollider.enabled = false;
            anim.PlayAnime(AnimState.death, 0, isFacingPlayer);
            AudioManager.am.PlayAudio(AudioLib.EnemyDeath);
            Invoke("Destroy", anim.anime.GetCurrentAnimatorStateInfo(0).length);
            callback = false;
        }
    }
    private void Destroy()
    {
        this.gameObject.SetActive(false);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(transform.position, Vector3.up, enemyStats.AttackRange, 1.5f);
    }
#endif
}
