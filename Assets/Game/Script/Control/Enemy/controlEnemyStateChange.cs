using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class controlEnemyStateChange : MonoBehaviour
{
    public enum AnimationState{ ArmStretching , Kiss, Defeat, NervouslyLookAround, OffensiveIdle, StandingW_BriefcaseIdle, UsingAFaxMachine }
    public AnimationState animeToRun;

    public string animationName;        
    public float rotationSmooth;

    public bool ismove = false;

    private cEFieldOfView FOV;
    private controlAnime anim;
    private controlEnemy enemy;
    
    private controlPlayer player;

    private NavMeshAgent agent;
    private Transform TargetTowardPlayer;
    private float turnSmoothVelocity;

    void Start()
    {
        anim = GetComponent<controlAnime>();
        FOV  = GetComponent<cEFieldOfView>();
        enemy = GetComponent<controlEnemy>();
        agent = GetComponent<NavMeshAgent>();
        player = FindObjectOfType<controlPlayer>();
        anim.PlayIdleAnim(AnimState.eIdle, animeToRun.ToString());
        Invoke("AnimatioDelay", 0.25f);
    }

    void Update()
    {
        checkIfPlayerInAttackRange();
    }
    void checkIfPlayerInAttackRange()
    {
        if (!enemy.isDead)
        {
            if (FOV.canSeeEnemy && agent.velocity.magnitude < 0.1f)
            {
                anim.PlayIdleAnim(AnimState.eIdle,"Idle");    
                
            }
            if (FOV.playerRef && player.isKilledEnemy)
            {
               ismove = true;
            }
            if (ismove) move();
        }
        anim.PlayAnime(AnimState.run, Mathf.Clamp(agent.velocity.magnitude, 0, 1));
    }
    void AnimatioDelay()
    {
        anim.PlayIdleAnim(AnimState.eIdle, animeToRun.ToString());
    }
    public void move()
    {
        if (FOV.playerRef) TargetTowardPlayer = FOV.playerRef.transform;
        if (TargetTowardPlayer)
        {
            if (Vector3.Distance(transform.position, TargetTowardPlayer.position) > 2)
            {
                agent.isStopped = false;
                agent.SetDestination(player.transform.position);
                movementControl();
            }
            if (Vector3.Distance(transform.position, TargetTowardPlayer.position) < 2)
            {
                agent.isStopped = true;
                ismove = false;
            }
        }
        if (!TargetTowardPlayer)
        {
            agent.isStopped = true;
            ismove = false;
        }
    }
    void movementControl()
    {
        if (agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }        
    }


}
