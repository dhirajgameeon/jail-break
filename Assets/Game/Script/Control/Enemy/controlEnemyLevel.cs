using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEnemyLevel : MonoBehaviour
{
    public GameObject[] Mesh;
    public bool P, Po, G;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        check();
    }

    void check()
    {
        if (P && !Mesh[0].activeSelf)
        {
            Mesh[0].SetActive(true);
            Mesh[1].SetActive(false);
            Mesh[2].SetActive(false);

        }
        if (Po && !Mesh[1].activeSelf)
        {
            Mesh[1].SetActive(true);
            Mesh[0].SetActive(false);
            Mesh[2].SetActive(false);

        }
        if (G && !Mesh[2].activeSelf)
        {
            Mesh[2].SetActive(true);
            Mesh[1].SetActive(false);
            Mesh[0].SetActive(false);

        }
    }
}
