using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioLib
{
    EnemyDeath,LevelComplete,LevelUp,Punch,PunchDoor
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager am;

    public AudioSource Source;
    public AudioClip[] audioData;

    private void Start()
    {
        am = this;
    }
    public void PlayAudio(AudioLib lib, int type=1)
    {
        switch (lib)
        {
            case AudioLib.EnemyDeath:
                Death();
                return;
            case AudioLib.LevelComplete:
                LevelComplete();
                return;
            case AudioLib.LevelUp:
                LevelUp();
                return;
            case AudioLib.Punch:
                Punch(type);
                return;
            case AudioLib.PunchDoor:
                PunchDoor();
                return;
        }
    }


    void Death()
    {
        Source.PlayOneShot(audioData[0]);
    }
    void LevelComplete()
    {
        Source.PlayOneShot(audioData[1]);
    }
    void LevelUp()
    {
        Source.PlayOneShot(audioData[2]);
    }
    void Punch(int type)
    {
        if(type==1)Source.PlayOneShot(audioData[3]);
        if(type==2)Source.PlayOneShot(audioData[4]);

    }
    void PunchDoor()
    {
        Source.PlayOneShot(audioData[5]);
    }
}
