using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cEFieldOfView : MonoBehaviour
{
    [Range(0,360)]
    public float angle = 45;
    public float radius;

    public GameObject playerRef;

    public LayerMask targetMask;
    public LayerMask otherMask;

    public bool canSeeEnemy;
    public bool facingEnemy;
    private void Start()
    {
        StartCoroutine(FOVRoutine());
    }

    IEnumerator FOVRoutine()
    {
        WaitForSeconds wait = new WaitForSeconds(0.2f);
        while (true)
        {
            yield return wait;
            FieldOfViewCheck();
        }
    }
    [SerializeField]Collider[] rangeChecks;
    private void FieldOfViewCheck()
    {
        /*Collider[] */rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);
        if (rangeChecks.Length > 0)
        {
            Transform target = rangeChecks[0].transform;                        
            playerRef = target.gameObject;            

            Vector3 directionToTarget = (target.position - transform.position).normalized;            
            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {               
                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, otherMask))
                {                    
                    canSeeEnemy = true;
                    facingEnemy = checkIfIsFacing();
                }
                else
                {                   
                    target = null;
                    //playerRef = null;
                    //facingEnemy = false;
                    //canSeeEnemy = false;
                    
                }
            }
            else
            {
                target = null;
                playerRef = null;
                facingEnemy = false;
                canSeeEnemy = false;
            }
        }
        else if (canSeeEnemy || playerRef)
        {
            playerRef = null;
            facingEnemy = false;
            canSeeEnemy = false;
        }
    }

    private bool checkIfIsFacing()
    {
        if (playerRef)
        {
            if (playerRef.GetComponent<cPFieldOfView>())
            {
                if (playerRef.GetComponent<cPFieldOfView>().enemyRef) return true;
                else
                    return false;
            }
        }
        else
            return false;

        return true;
    }

}
