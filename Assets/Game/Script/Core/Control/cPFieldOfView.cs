using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cPFieldOfView : MonoBehaviour
{
    [Range(0,360)]
    public float angle = 45;
    public float radius;

    public GameObject enemyRef;
    public GameObject doorRef;

    public LayerMask targetMask;
    public LayerMask doorMask;
    public LayerMask otherMask;

    public bool canSeeEnemy;
    public bool canSeeDoor;
    public bool facingEnemy;
    private void Start()
    {
        //StartCoroutine(FOVRoutine());
    }

/*    IEnumerator FOVRoutine()
    {
        WaitForSeconds wait = new WaitForSeconds(0.2f);
        while (true)
        {
            yield return wait;
            FieldOfViewCheck();
        }
    }*/

    [SerializeField]Collider[] rangeChecks;
    private void FieldOfViewCheck()
    {
        /*Collider[]*/ rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);
        if (rangeChecks.Length != 0)
        {
            Transform target = rangeChecks[rangeChecks.Length - 1].transform;         
            enemyRef = target.gameObject;
            //print("gotData");
            Vector3 directionToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, otherMask))
                {
                    canSeeEnemy = true;
                    facingEnemy = checkIfIsFacing();
                }
                else if(canSeeEnemy)
                {
                    enemyRef = null;
                    target = null;
                    facingEnemy = false;
                    canSeeEnemy = false;
                    
                }
            }
            else if(canSeeEnemy)
            {
                target = null;
                enemyRef = null;
                facingEnemy = false;
                canSeeEnemy = false;
            }
        }
        else if (canSeeEnemy)
        {
            facingEnemy = false;
            canSeeEnemy = false;
        }
        if(rangeChecks.Length == 0)
        {
            enemyRef=null;  
        }


        Collider[] checkForDoor = Physics.OverlapSphere(transform.position, radius, doorMask);
        if (checkForDoor.Length != 0)
        {
            Transform target = checkForDoor[checkForDoor.Length - 1].transform;
            doorRef = target.gameObject;
            Vector3 directionToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, otherMask))
                {
                    canSeeDoor = true;
                }
                else if (canSeeDoor)
                {
                    target = null;
                    canSeeDoor = false;

                }
            }
            else if (canSeeDoor)
            {
                target = null;
                doorRef = null;
                canSeeDoor = false;
            }
        }
        else if (canSeeDoor)
        {
            doorRef = null;
            canSeeDoor = false;
        }
        if (checkForDoor.Length == 0)
        {
            doorRef = null;
        }
    }
    private bool checkIfIsFacing()
    {
        if (enemyRef)
        {
            if (enemyRef.GetComponent<cEFieldOfView>())
            {
                if (enemyRef.GetComponent<cEFieldOfView>().playerRef) return true;
                else return false;
            }
        }
        else return false;

        return true;
    }

    private void Update()
    {
        FieldOfViewCheck();
    }
}
