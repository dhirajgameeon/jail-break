using UnityEngine;

[System.Serializable]
public class Stats
{
    public int Level;
    public float MovementSpeed;
    public float movementSpeedWhileAttacking;
    public float AttackRange;
    public float AttackSpeed;
    public float AttackRate;

    public Stats()
    {
        Level = 1;
        AttackRange = 3;
        AttackSpeed = 1;
        MovementSpeed = 5;
        movementSpeedWhileAttacking = 2;
    }
}
