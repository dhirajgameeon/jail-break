using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int Cash;
    [Header("Progress Slider")]
    public Slider Progress;
    public float upSpeed = 10;
    private float currentProgress;



    public List<bool> isDoorOpen = new List<bool> { false };
    public float DoorComplete;

    [Header("Player Status")]
    [Space(7)]
    public GameObject Continue;
    public GameObject Retry;
    public bool isPlayerDead;
    public bool isGameComplete;

    [Header("OnGameComplete")]
    public Animator cinemachineCamera;
    public GameObject Confetti;

    void Start()
    {
        instance = this;
        Progress.maxValue = isDoorOpen.Count;
        Confetti.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Progress.value = DoorComplete;
        progress();
        onGameComplete();
    }

    void progress()
    {
        foreach(var item in isDoorOpen.ToArray())
        {
            if (item)
                isDoorOpen.Remove(item);
        }

        if(currentProgress< 5 - isDoorOpen.Count)
        {
            currentProgress += upSpeed * Time.deltaTime;
            if(currentProgress>= 5 - isDoorOpen.Count) 
                currentProgress = 5- isDoorOpen.Count;
        }

        Progress.value = currentProgress;
    }

    void onGameComplete()
    {
        if (isGameComplete)
        {
            if (!cinemachineCamera.GetCurrentAnimatorStateInfo(0).IsName("Zoomed Cam"))
            {
                AudioManager.am.PlayAudio(AudioLib.LevelComplete);
                cinemachineCamera.Play("Zoomed Cam");
                Confetti.SetActive(true);
            }
        }
    }
}
