using UnityEngine;
using Facebook.Unity;

public class SDKManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
        FB.Init();
    }
}
