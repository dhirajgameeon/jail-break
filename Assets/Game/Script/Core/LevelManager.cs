using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public int Level;
    public TextMeshProUGUI LevelText;

    
    void Start()
    {
        //resetData();
        Level = LoadData("Level");
        LevelText.text = "Level : " + (Level + 1).ToString("N0");
    }


    void Update()
    {
        
    }
    public void NextLevel()
    {
        Level++;
        SaveData(Level, "Level");
        SceneManager.LoadScene(Level % 3);
        //SceneManager.LoadScene(0);

    }
    public void RetryLevel()
    {
        SaveData(Level, "Level");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void resetData()
    {
        SaveData(0, "Level");
    }
    void SaveData(int Data, string key)
    {
        PlayerPrefs.SetInt(key, Level);
    }
    int LoadData(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetInt(key);
        }
        return 0;
    }
}
