using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class movePlayer : MonoBehaviour
{
    public Joystick Joystick;
    public Transform groundC;
    public float rotationSmooth;

    [HideInInspector]public float movementSpeed;
    private float turnSmoothVelocity;
    [HideInInspector] public Vector3 direction;

    private CharacterController CharacterController;
    private controlAnime anime;
    private controlPlayer player;
    void Start()
    {
        CharacterController = GetComponent<CharacterController>();
        anime = GetComponent<controlAnime>();
        player = GetComponent<controlPlayer>();
    }


    void Update()
    {
        if(!player.isDied)
            MovePlayer();
    }


    public void MovePlayer()
    {
        float x = Joystick.Horizontal;
        float z = Joystick.Vertical;

        direction = new Vector3(x, 0, z).normalized;

        if (direction.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle + 45, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * new Vector3(1, 0, 1);
            CharacterController.Move(moveDir.normalized * movementSpeed * Time.deltaTime);
        }


        CharacterController.Move(-transform.up * 5 * Time.deltaTime);
        anime.PlayAnime(AnimState.idle, direction.magnitude);
        anime.PlayAnime(AnimState.run, direction.magnitude);
    }
}
