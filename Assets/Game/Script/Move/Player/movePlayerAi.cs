using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayerAi : MonoBehaviour
{
    public static movePlayerAi MPA;
    public movePlayer movePlayer;
    public GameObject LevelIndecator;

    public Transform finalPositionToMove;
    public Transform doorPositionToLook;

    public bool isPlayerOpenLastDoor;
    private controlAnime anim;
    void Start()
    {
        MPA = this;
        anim = GetComponent<controlAnime>();
    }
    //GameManager.instance.Continue.SetActive(true);
    void Update()
    {
        moveToLastPoint();
    }

    void moveToLastPoint()
    {
        
        if (isPlayerOpenLastDoor && !GameManager.instance.isGameComplete)
        {
            movePlayer.enabled = false;
            if (Vector3.Distance(transform.position, finalPositionToMove.position)<0.5f)
            {
                smoothLookat(doorPositionToLook, 100);
                GameManager.instance.isGameComplete = true;               
                GameManager.instance.Continue.SetActive(true);
                anim.PlayAnime(AnimState.deance, 0);
            }
            else
            {
                LevelIndecator.SetActive(false);
                transform.position = Vector3.Lerp(transform.position, finalPositionToMove.position, 1.5f * Time.deltaTime);
                smoothLookat(finalPositionToMove, 50);
                anim.PlayAnime(AnimState.run, 1);
            }
        }
    }
    void smoothLookat(Transform target, float rotationSpeed)
    {
        var targetPos = target.position;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }
}
