using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class stopWatch : MonoBehaviour
{
    float currentTime;
    int startM;
    [SerializeField]TextMeshProUGUI text;
    void Start()
    {
        currentTime = 0;

    }

    void Update()
    {
        currentTime = currentTime + Time.deltaTime;
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        text.text = time.Minutes + ":" + time.Seconds.ToString();
    }
}
